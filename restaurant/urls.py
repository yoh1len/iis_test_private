from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^menu/$',views.menu,name='menu'),
    url(r'^get_food/$',views.get_food,name='get_food'),
    url(r'^del_food/$',views.del_food,name='del_food'),
    url(r'^success/$',views.success,name='success'),
    url(r'^chyba/$',views.chyba,name='chyba'),
    url(r'^order/$',views.current_order,name='current_order'),
    url(r'^conf_order/$',views.confirm_order,name='confirm_order'),
]