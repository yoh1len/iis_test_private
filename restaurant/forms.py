from django import forms
from .models import Order
from django.db import models
from django.forms import ModelForm

class FoodOrder(forms.Form):
    quantity = forms.IntegerField()
    food_type = forms.IntegerField()
       
class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ['name', 'surname', 'email', 'mobile', 'type_order', 'price']
        widgets = {'price': forms.HiddenInput()}
        