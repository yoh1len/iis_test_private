from django.db import models

class Room(models.Model):
    capacity = models.SmallIntegerField("Kapacita miestnosti")
    def __str__(self):
        return 'Room %d has capacity: %d' % (self.id, self.capacity)
    
class Table(models.Model):
    room_id = models.ForeignKey(Room, on_delete=models.CASCADE, verbose_name="Cislo miestnosti, kde sa nachadza stol")
    chairs = models.SmallIntegerField("Pocet stoliciek pri stole")
    def __str__(self):
        return 'Table %d has %d chairs' % (self.id, self.chairs)
    
class User(models.Model):
    REGULAR = 'R'
    EMPLOYEE = 'E'
    type_user_choices = (
        (REGULAR, 'Zakaznik'),
        (EMPLOYEE, 'Zamestnanec'),
    )
    name = models.CharField("Meno", max_length=30)
    surname = models.CharField("Priezvisko", max_length=30)
    address = models.CharField("Adresa", max_length=50)
    email = models.CharField("E-mail", max_length=20)
    mobile = models.CharField("Tel. cislo", max_length=15)
    type_User = models.CharField("Typ uzivatela", max_length=1, choices=type_user_choices, default=REGULAR) 
    def __str__(self):
        return 'User: %s %s Type: %s Email: %s' % (self.name, self.surname, self.type_User, self.email)

class Food(models.Model):
    name = models.CharField("Nazov jedla", max_length=50)
    price = models.SmallIntegerField("Cena jedla")
    weight = models.SmallIntegerField("Hmostnost jednej porcie")
    picture = models.ImageField("Obrazok")
    ingredients = models.CharField("Hlavne ingrediencie jedla", max_length=180)
    def __str__(self):
        return 'Food: %s Price: %d' % (self.name, self.price)
    
class Reservation(models.Model):
    table_id = models.ForeignKey(Table, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Cislo rezervovaneho stolu, ak cela miestnost, tak nechat prazdne")
    room_id = models.ForeignKey(Room, on_delete=models.CASCADE, blank=True, null=True, verbose_name="Cislo rezervovanej miestnosti, ak len stol, tak nechat prazdne")
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, verbose_name="ID uzivatela, ktory si rezervuje")
    price = models.SmallIntegerField("Celkova cena rezervacie")
    date = models.DateField("Datum rezervacie")
    time_from = models.TimeField("Cas od ktorej je rezervovane")
    time_to = models.TimeField("Cas do ktoreho je rezervovane")
    name = models.CharField("Meno", max_length=30)
    surname = models.CharField("Priezvisko", max_length=30)
    email = models.CharField("E-mail", max_length=20)
    mobile = models.CharField("Tel. cislo", max_length=15)
    def __str__(self):
        return 'Table: %d User: %s %s Date: %s' % (self.table_id, self.name, self.surname, self.date)

class Order(models.Model):
    HERE = 'H'
    DELIVERY = 'D'
    type_order_choice = (
        (DELIVERY, 'Rozvoz'),
        (HERE, 'Na stol'),
    )
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True,verbose_name="ID objednavajuceho zakaznika, moze by prazdne")
    table_id = models.ForeignKey(Table, on_delete=models.CASCADE, blank=True, null=True, verbose_name="ID stola ku ktoremu patri objednavka, moze byt prazdne")
    price = models.SmallIntegerField("Celkova suma objednavky")
    type_order = models.CharField("Typ objednavky", max_length=1, choices=type_order_choice, default=HERE)
    name = models.CharField("Meno", max_length=30)
    surname = models.CharField("Priezvisko", max_length=30)
    email = models.CharField("E-mail", max_length=20)
    mobile = models.CharField("Tel. cislo", max_length=15)
    food_list = models.ManyToManyField(Food, through='Order_Food', through_fields=('order_id', 'food_id'))
    def __str__(self):
        return 'User: %s %s' % (self.name, self.surname)
    
class Order_Food(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE, verbose_name="ID objednavky")
    food_id = models.ForeignKey(Food, on_delete=models.CASCADE, verbose_name="ID jedla")
    amount = models.SmallIntegerField("Mnozstvo daneho jedla")
    def __str__(self):
        return '%s %s Amount: %d' % (self.food_id, self.order_id, self.amount)
    

