from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from .models import Food, Order, Order_Food
from .forms import FoodOrder, OrderForm

def index(request):
    template = loader.get_template('restaurant/index.html')
    request.session.flush()
    return HttpResponse(template.render(request))
def menu (request):
    all_food_list = Food.objects.order_by('id')
    template = loader.get_template('restaurant/menu.html')
    context = {
        'all_food_list': all_food_list,
    }
    return HttpResponse(template.render(context, request))
def get_food(request):
    if request.method == 'POST':
        form = FoodOrder(request.POST)
        if form.is_valid():
        #Spracovanie 
            quantity = form.cleaned_data['quantity']
            #Food_type je ID daneho jedla
            food_type = str(form.cleaned_data['food_type'])
            print('Food Type ID Start: {}'.format(food_type))
            print('Food Quantity Start: {}'.format(quantity))
            print('Session pre insert {}:  (Pre Insert from session)'.format(request.session.items()))
            #pridanie do session id jedla a mnozstva
            #TODO: funguje akurat sa to neprepisuje  takze dorobit prepisovanie
            #try:
                #del request.session[food_type]
               # print('S_UPD: Food ID {} quantity: {} (pre_del)'.format(food_type,request.session[food_type]))
                #request.session[food_type] = quantity
               # request.session.modified = True
              #  print('S_UPD: Food ID {} quantity: {} (post_del_in)'.format(food_type,request.session[food_type]))
             #   return HttpResponseRedirect('/success/')
            #    request.session.modified = True	
           # except KeyError:
            request.session[food_type] = quantity
            print('S_NEW: Food ID {} quantity: {}'.format(food_type,request.session[food_type]))
            request.session.modified = True
            print('Session post insert {}:  (Post Insert from session)'.format(request.session.items()))
            return HttpResponseRedirect('/menu/')
            
            
          #  else:
          #      return HttpResponseRedirect('/chyba/')
          #                  
        else:
            return HttpResponseRedirect('/chyba/')
def del_food(request):
    return HttpResponse("TODO: Vytvorit formular a spracovanie v tomto view. Delete view.")
def success(request):
    return HttpResponse("Success.")
def chyba(request):
    return HttpResponse("Chyba")
def current_order(request):
    #template = loader.get_template('restaurant/current_order.html')
    kluce = request.session.keys()
    items = request.session.items()
    zoznam = list(Food.objects.filter(id__in=(kluce)))
    spolu = 0
    form = OrderForm()
    for kluc in kluce:
        #praca s danym klucom, vybratie z DB, priradenie poctu, blah blah...
        jedlo = Food.objects.get(pk=kluc)
        cena = jedlo.price
        mnozstvo = request.session[kluc]
        spolu += cena * mnozstvo
    
    context = {
        'zoznam': zoznam,
        'spolu': spolu,
        'items': items,
        'form': form,
    }
    
    return render(request, 'restaurant/current_order.html', context)
def confirm_order(request):
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            surname = form.cleaned_data['surname']
            email = form.cleaned_data['email']
            mobile = form.cleaned_data['mobile']
            price = form.cleaned_data['price']
            type_order = form.cleaned_data['type_order']
            
            new_order = Order(type_order=type_order,name=name,surname=surname,email=email,mobile=mobile,price=price)
            new_order_id = new_order.save()
            
            food_ids = request.session.keys()
            
            for food_id in food_ids:
                quantity = request.session[food_id]
                new_order_food = Order_Food(order_id=Order.objects.get(pk=new_order.pk), food_id=Food.objects.get(pk=food_id),amount=quantity)
                new_order_food_id = new_order_food.save()
            
            
            print('Order ID: {} {} {} Email: {} Mobile: {} Type: {} Price: {} Food List: {}'.format(new_order.pk,name,surname,email,mobile,type_order, price, new_order.food_list.all()))
            return HttpResponseRedirect('/success/')
            #Po tade funguje
        else:
            return HttpResponseRedirect('/chyba/')
    return render(request,'restaurant/conf_order.html', {'form': form})
# Create your views here.
