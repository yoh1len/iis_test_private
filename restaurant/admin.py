from django.contrib import admin

from .models import Room, Table, User, Food, Reservation, Order, Order_Food

admin.site.register(Room)
admin.site.register(Table)
admin.site.register(User)
admin.site.register(Food)
admin.site.register(Reservation)
admin.site.register(Order)
admin.site.register(Order_Food)

# Register your models here.
